package at.technikum.simpleuserinterface.tasks;

import android.os.AsyncTask;

import java.io.IOException;
import java.net.InetSocketAddress;
import java.net.Socket;
import java.net.SocketAddress;

import at.technikum.simpleuserinterface.MainActivity;

public class HostAvailabilityTask extends AsyncTask<String, Void, Boolean> {

    private final MainActivity main;

    public HostAvailabilityTask(MainActivity main) {
        this.main = main;
    }

    /**
     * Check if the device has an connection to the internet
     * by creating a socket connection to the google DNS server
     *
     * @param params IP Address or websites to check
     * @return if internet connection does exists
     */
    @Override
    protected Boolean doInBackground(String... params) {
        SocketAddress socketAddress = new InetSocketAddress(params[0], 53);
        Socket socket = new Socket();

        try {
            socket.connect(socketAddress, 500);
        } catch (Exception e) {
            return false;
        } finally {
            try {
                socket.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
        }

        return true;
    }

    /**
     * Executed after socket connection test
     *
     * @param isConnected if connection was successful
     */
    @Override
    protected void onPostExecute(Boolean isConnected) {
        if (!isConnected) main.showInternetStatusDialog(isConnected);
    }

}