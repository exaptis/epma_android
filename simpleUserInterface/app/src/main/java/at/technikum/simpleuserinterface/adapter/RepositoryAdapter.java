package at.technikum.simpleuserinterface.adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.TextView;

import java.util.List;

import at.technikum.simpleuserinterface.R;
import at.technikum.simpleuserinterface.domain.Repository;

public class RepositoryAdapter extends ArrayAdapter<Repository> {
    public RepositoryAdapter(Context context, List<Repository> repositories) {
        super(context, 0, repositories);

    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {

        String repositoryName = getItem(position).getName();
        String repositoryLanguage = getItem(position).getLanguage();

        if (convertView == null) {
            convertView = LayoutInflater.from(getContext()).inflate(R.layout.item_repositoriy, parent, false);
        }

        TextView textViewName = (TextView) convertView.findViewById(R.id.github_repository_name);
        TextView textViewLanguage = (TextView) convertView.findViewById(R.id.github_repository_language);

        textViewName.setText(repositoryName);
        textViewLanguage.setText(repositoryLanguage);

        return convertView;
    }
}

