package at.technikum.simpleuserinterface.service;

import java.util.List;

import at.technikum.simpleuserinterface.domain.Contributor;
import at.technikum.simpleuserinterface.domain.Repository;
import retrofit.Callback;
import retrofit.http.GET;
import retrofit.http.Path;

public interface GitHubService {
    @GET("/repos/{owner}/{repo}/contributors")
    void listContributors(
            @Path("owner") String owner,
            @Path("repo") String repo,
            Callback<List<Contributor>> cb
    );

    @GET("/users/{user}/repos")
    void listRepositories(
            @Path("user") String user,
            Callback<List<Repository>> cb
    );

    @GET("/users/{user}")
    void getUser(
            @Path("user") String user,
            Callback<Contributor> cb
    );
}