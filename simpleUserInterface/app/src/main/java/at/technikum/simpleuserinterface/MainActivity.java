package at.technikum.simpleuserinterface;

import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.Fragment;
import android.app.FragmentTransaction;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.DialogInterface;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.os.Bundle;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.EditText;
import android.widget.Toast;

import com.nostra13.universalimageloader.core.ImageLoader;
import com.nostra13.universalimageloader.core.ImageLoaderConfiguration;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.List;

import at.technikum.simpleuserinterface.domain.Contributor;
import at.technikum.simpleuserinterface.domain.Repository;
import at.technikum.simpleuserinterface.fragments.RepositoryListFragment;
import at.technikum.simpleuserinterface.fragments.TextInputFragment;
import at.technikum.simpleuserinterface.service.GitHubService;
import at.technikum.simpleuserinterface.tasks.HostAvailabilityTask;
import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.RetrofitError;
import retrofit.client.Response;

public class MainActivity extends Activity {

    private GitHubService github;

    /**
     * Set the default layout, enable the action bar and render the text input fragment
     *
     * @param savedInstanceState the currently saved instance state (upon orientation change for example)
     */
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        createGithubService();

        if (getActionBar() != null) {
            getActionBar().setDisplayHomeAsUpEnabled(true);
        }

        if (savedInstanceState == null) {
            renderTextInputFragment(false);
        }

        ImageLoaderConfiguration config = new ImageLoaderConfiguration.Builder(this).build();
        ImageLoader.getInstance().init(config);

        new HostAvailabilityTask(this).execute("8.8.4.4");
    }

    /**
     * Inflate the menu; this adds items to the action bar if it is present.
     *
     * @param menu the menu to be inflated
     * @return always true
     */
    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.main_activity_actions, menu);
        return true;
    }

    /**
     * Trigger according actions for the action bar.
     * If no action is registered for the item id then the onBackPressed action is executed.
     *
     * @param item the pressed menu item from the action bar
     * @return operation success
     */
    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            case R.id.action_search:
                showToast("Search!");
                return true;
            case R.id.action_settings:
                renderTextInputFragment(true);
                return true;
            case android.R.id.home:
                onBackPressed();
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }
    }

    /**
     * When the back button is pressed, traverse the fragment history upwards.
     * As the first fragment is the initial frame, we do not want to skip it,
     * therefore popBackStack is only called with a BackStackEntryCount > 1
     */
    @Override
    public void onBackPressed() {
        if (getFragmentManager().getBackStackEntryCount() > 1) {
            getFragmentManager().popBackStack();
        } else {
            this.finish();
        }
    }

    private void createGithubService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint("https://api.github.com")
                .build();

        github = restAdapter.create(GitHubService.class);
    }

    /**
     * Create a new Dialog window
     *
     * @param title the resource key of the title to show
     * @param message the resource key of the message to show
     * @param icon the resource key of the icon to show
     * @return the created dialog instance
     */
    public Dialog createDialog(int title, int message, int icon) {

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setIcon(icon)
                .setMessage(message)
                .setCancelable(false)
                .setPositiveButton(R.string.button_ok, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int id) {
                        dialog.dismiss();
                    }
                });

        return builder.create();
    }

    /**
     * Create a new List Dialog containing contributors
     *
     * @param title     the resource key of the title to show
     * @param listItems the contributors to show
     * @return dialog instance
     */
    public Dialog createContributorsDialog(int title, ArrayList<String> listItems) {
        final CharSequence[] items = listItems.toArray(new CharSequence[listItems.size()]);

        AlertDialog.Builder builder = new AlertDialog.Builder(this);
        builder.setTitle(title)
                .setItems(items, new DialogInterface.OnClickListener() {
                    public void onClick(DialogInterface dialog, int which) {
                        String githubUsername = (String) items[which];
                        showRepositoriesByUsername(githubUsername);
                    }
                });
        return builder.create();
    }

    /**
     * Create a new Progress Dialog window
     *
     * @param title dialog title
     * @return dialog instance
     */
    public ProgressDialog createProgressDialog(int title) {

        ProgressDialog progressDialog = new ProgressDialog(this);
        progressDialog.setTitle(title);
        progressDialog.setMessage("Please wait ...");
        progressDialog.setCancelable(false);

        return progressDialog;
    }

    /**
     * Retrieve github username from input and load display fragment
     *
     * @param view from within the method was triggered
     */
    public void lookupRepositories(View view) {
        EditText editText = (EditText) findViewById(R.id.github_username);
        final String username = editText.getText().toString();
        showRepositoriesByUsername(username);
    }

    /**
     * Show the contributors for a given repository
     *
     * @param contributor the github user object
     * @param repository  the github repository to show the contributors for
     */
    public void showContributorsByRepository(final Contributor contributor, final String repository) {
        final ProgressDialog progressDialog = createProgressDialog(R.string.dialog_processing_request_title);
        progressDialog.show();

        github.listContributors(contributor.getLogin(), repository, new Callback<List<Contributor>>() {

            @Override
            public void success(List<Contributor> contributors, Response response) {
                progressDialog.hide();

                ArrayList<String> contributorList = new ArrayList<String>();

                for (Contributor contributor : contributors) {
                    contributorList.add(contributor.getLogin());
                }

                createContributorsDialog(R.string.dialog_contributor_title, contributorList).show();
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.hide();
            }
        });
    }


    /**
     * Display the repositories by github username
     *
     * @param username the github username
     */
    public void showRepositoriesByUsername(final String username) {
        final ProgressDialog progressDialog = createProgressDialog(R.string.dialog_processing_request_title);
        progressDialog.show();

        // First try to load the user
        github.getUser(username, new Callback<Contributor>() {
            @Override
            public void success(final Contributor contributor, Response response) {

                // Second try to load the repositories
                github.listRepositories(contributor.getLogin(), new Callback<List<Repository>>() {

                    @Override
                    public void success(List<Repository> repositories, Response response) {
                        renderRepositoryListFragment(contributor, repositories);
                        progressDialog.dismiss();
                        showToast(repositories.size() + " repositories");
                    }

                    @Override
                    public void failure(RetrofitError error) {
                        progressDialog.dismiss();
                        createDialog(R.string.dialog_error_title, R.string.dialog_error_username_not_found, R.drawable.fail).show();
                    }
                });
            }

            @Override
            public void failure(RetrofitError error) {
                progressDialog.dismiss();
                createDialog(R.string.dialog_error_title, R.string.dialog_error_username_not_found, R.drawable.fail).show();
            }
        });


    }

    /**
     * Show info message
     *
     * @param text text to be displayed
     */
    public void showToast(CharSequence text) {
        Toast toast = Toast.makeText(getApplicationContext(), text, Toast.LENGTH_SHORT);
        toast.show();
    }

    /**
     * Display the Internet Status Dialog
     * @param isConnected determine if success or error message should be shown
     */
    public void showInternetStatusDialog(boolean isConnected) {
        int message = isConnected ? R.string.dialog_internet_connection_message : R.string.dialog_internet_no_connection_message;
        int icon = isConnected ? R.drawable.success : R.drawable.fail;
        Dialog dialog = createDialog(R.string.dialog_internet_connection_title, message, icon);
        dialog.show();
    }


    /**
     * Render the text input fragment
     *
     * @param replaceFragment toggle if fragment should be added or replaced
     */
    private void renderTextInputFragment(boolean replaceFragment) {
        Fragment textInputFragment = new TextInputFragment();
        showFragment(R.id.container, textInputFragment, replaceFragment);
    }

    /**
     * Render the text repository list fragment
     *
     * @param contributor  the github user object
     * @param repositories the list of github repositories
     */
    private void renderRepositoryListFragment(Contributor contributor, List<Repository> repositories) {
        Fragment repositoryListFragment = new RepositoryListFragment();
        Bundle args = new Bundle();
        args.putParcelable(RepositoryListFragment.ARG_REPOSITORIES, Parcels.wrap(repositories));
        args.putParcelable(RepositoryListFragment.ARG_USER, Parcels.wrap(contributor));
        repositoryListFragment.setArguments(args);

        showFragment(R.id.container, repositoryListFragment, true);
    }

    /**
     * Show the current fragment on the given container and add it to the stack for back navigation
     *
     * @param containerViewId fragment target container
     * @param fragment        fragment to be displayed
     * @param replaceFragment toggle if fragment should be added or replaced
     */
    private void showFragment(int containerViewId, Fragment fragment, boolean replaceFragment) {
        FragmentTransaction fragmentTransaction = getFragmentManager().beginTransaction();

        if (replaceFragment)
            fragmentTransaction.replace(containerViewId, fragment);
        else
            fragmentTransaction.add(containerViewId, fragment);

        fragmentTransaction.addToBackStack(null).commit();
    }

    /**
     * Check if the device has an active network connection
     *
     * @return if network connection does exists
     */
    private boolean isNetworkConnected() {
        ConnectivityManager connectivityManager = (ConnectivityManager) getSystemService(Context.CONNECTIVITY_SERVICE);
        NetworkInfo networkInfo = connectivityManager.getActiveNetworkInfo();
        return networkInfo != null && networkInfo.isConnectedOrConnecting();
    }

}
