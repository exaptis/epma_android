package at.technikum.simpleuserinterface.domain;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Parcel
@NoArgsConstructor
public class Repository {

    @Getter
    @Setter
    private String name;

    @Getter
    @Setter
    private String language;

    @Getter
    @Setter
    private Contributor owner;
}