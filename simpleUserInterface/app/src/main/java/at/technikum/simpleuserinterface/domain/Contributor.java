package at.technikum.simpleuserinterface.domain;

import com.google.gson.annotations.SerializedName;

import org.parceler.Parcel;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@Parcel
@NoArgsConstructor
public class Contributor {

    @Getter
    @Setter
    private String login;

    @Getter
    @Setter
    private int contributions;

    @Getter
    @Setter
    @SerializedName("avatar_url")
    private String avatarUrl;

    @Getter
    @Setter
    private String followers;
}