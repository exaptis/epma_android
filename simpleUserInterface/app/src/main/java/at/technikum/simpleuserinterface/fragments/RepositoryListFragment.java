package at.technikum.simpleuserinterface.fragments;

import android.app.Fragment;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.TextView;

import com.nostra13.universalimageloader.core.ImageLoader;

import org.parceler.Parcels;

import java.util.List;

import at.technikum.simpleuserinterface.MainActivity;
import at.technikum.simpleuserinterface.R;
import at.technikum.simpleuserinterface.adapter.RepositoryAdapter;
import at.technikum.simpleuserinterface.domain.Contributor;
import at.technikum.simpleuserinterface.domain.Repository;


public class RepositoryListFragment extends Fragment {
    public static final String ARG_USER = "USERNAME";
    public static final String ARG_REPOSITORIES = "REPOSITORIES";

    @Override
    public View onCreateView(LayoutInflater inflater, final ViewGroup container, Bundle savedInstanceState) {

        // Get string from arguments
        Bundle arguments = getArguments();
        View view = inflater.inflate(R.layout.fragment_repository_list, container, false);

        if (arguments != null && view != null) {
            // Get message from arguments
            final Contributor contributor = Parcels.unwrap(arguments.getParcelable(ARG_USER));
            final List<Repository> repositoryList = Parcels.unwrap(arguments.getParcelable(ARG_REPOSITORIES));

            RepositoryAdapter repositoryAdapter = new RepositoryAdapter(getActivity(), repositoryList);

            // Create the text view
            TextView loginTextView = (TextView) view.findViewById(R.id.github_username_label);
            loginTextView.setText(contributor.getLogin());

            TextView followersTextView = (TextView) view.findViewById(R.id.github_followers);
            followersTextView.setText(contributor.getFollowers());

            ImageView imageView = (ImageView) view.findViewById(R.id.github_avatar);
            ImageLoader.getInstance().displayImage(contributor.getAvatarUrl(), imageView);

            // Create the list view
            ListView listView = (ListView) view.findViewById(R.id.github_repositories);
            listView.setAdapter(repositoryAdapter);
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {
                    TextView itemTextView = (TextView) view.findViewById(R.id.github_repository_name); // get the child text view
                    final String repository = itemTextView.getText().toString();

                    ((MainActivity) getActivity()).showContributorsByRepository(contributor, repository);
                }
            });
        }

        return view;
    }

}
